/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.threadtowritenames;

/**
 *
 * @author tbiro
 */
public class NamePrinterThread extends Thread {
    private String name;
    private int howMany;

    public NamePrinterThread(String name, int howMany) {
        this.name = name;
        this.howMany = howMany;
    }
    
    public String getNameb() {
        return name;
    }

    public void setNameb(String name) {
        this.name = name;
    }

    public int getHowMany() {
        return howMany;
    }

    public void setHowMany(int howMany) {
        this.howMany = howMany;
    }

    @Override
    public void run() {
        for (int i=1;i<=this.howMany;i++){
        System.out.println("A neved: "+this.name+" "+i+".kiírás. Összesen: "+this.howMany+" -ből.");    
    }

}
}