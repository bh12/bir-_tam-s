insert into company (id,name) values (-5,"Hungary");
insert into company (id,name) values (-6,"Bolivia");
insert into site (id,address,company_id) values (-9, "Budapest Kálin tér",-5);
insert into site (id,address,company_id) values (-13, "Budapest Lónyai tér",-6);
insert into car(carid, color, site_id) values (-10,'fekete',-13);
insert into car(carid, color, site_id) values (-11,'barna',-9);
insert into car(carid, color, site_id) values (-12,'fehér',-13);