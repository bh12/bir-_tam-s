package braininghub.dao;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-23T13:48:30")
@StaticMetamodel(Car.class)
public class Car_ { 

    public static volatile SingularAttribute<Car, String> details;
    public static volatile SingularAttribute<Car, Long> id;
    public static volatile SingularAttribute<Car, Integer> carId;

}