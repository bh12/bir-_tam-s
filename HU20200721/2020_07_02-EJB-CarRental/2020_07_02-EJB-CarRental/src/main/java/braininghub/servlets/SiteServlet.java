package braininghub.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SiteServlet")
public class SiteServlet extends HttpServlet {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/braininghub?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("site.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Integer carId = Integer.valueOf(req.getParameter("siteId"));
        String details = req.getParameter("address");

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SiteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        String insertQuery = "insert into site(site_id, address) values(?, ?)";
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL, "admin", "admin");
                PreparedStatement st = conn.prepareStatement(insertQuery);) {
            st.setInt(1, carId);
            st.setString(2, details);
            
            int rowsCreated = st.executeUpdate();
            
            System.out.println(rowsCreated + " sites created.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        res.sendRedirect("MainMenuServlet");
    }
}
