package braininghub.dao;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class CarDAO {

    @PersistenceContext
    EntityManager em; //ez felelős az adatbázis "kapcsolatért"?
    
    public Cars newCar;

    public CarDAO() {
    }

    public CarDAO(Cars newCar) {
        this.newCar = newCar;
    }
    
    public void addCar(Cars newCar) {
        em.persist(newCar);
    }

}

/*
@Stateless //régi bean-es
public class CarDAO {
    
    @Resource(name = "jdbc/bh")
    DataSource ds;
    
    public void addCar(int carId, String details) {
        String insertQuery = "insert into car(car_id, details) values(?, ?)";
        try (Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement(insertQuery);) {
            st.setInt(1, carId);
            st.setString(2, details);
            
            int rowsCreated = st.executeUpdate();
            
            System.out.println(rowsCreated + " cars created.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
 */
