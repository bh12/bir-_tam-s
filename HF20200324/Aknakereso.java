/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aknakereso;

import java.util.Scanner;

public class Aknakereso {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Mekkora pályaméret legyen a játékhoz? Adj meg függőleges, majd vízszintes mező méreteket!");
        int n = 0, m = 0;
        do {
            System.out.println("Ne legyen 2 mező alatt!");
            n = sc.nextInt();
            m = sc.nextInt();
        } while (n < 2 || m < 2);
        boolean[][] field = new boolean[n][m];
        boolean gameWithMines[][] = randomMines(field);
        printField(gameWithMines);
        int life=3;
        game(gameWithMines,life);
    }

    public static boolean[][] randomMines(boolean[][] field) {
        int xRandom, yRandom;
        int nRandom = (int) (Math.random() * (field.length / 2.0 - field.length / 4.0) + field.length / 4.0 + 1);
        //System.out.println(nRandom);
        int mRandom = (int) (Math.random() * (field[0].length / 2.0 - field[0].length / 4.0) + field[0].length / 4.0 + 1);
        //System.out.println(mRandom);
        int randomMineCounter = nRandom * mRandom;
        for (int i = 0; i < randomMineCounter; i++) {
            do {
                xRandom = (int) (Math.random() * field.length);
                yRandom = (int) (Math.random() * field[0].length);
            } while (field[xRandom][yRandom] == true);
            field[xRandom][yRandom] = true; // akna ehelyezése
        }
        return field;
    }

    public static void game(boolean field[][],int life) {
        System.out.println("Adj meg koordinátákat a mezőn belül! A bal felső sarok 1,1. Először függőleges, majd vízszintes koordináta! Ne lépj aknára!");
        Scanner sc = new Scanner(System.in);
        int x, y;
        int counter = 1;
        do {
            do {
                System.out.println("Mi az " + counter + ". tipped?");
                x = sc.nextInt() - 1;
                y = sc.nextInt() - 1;
            } while (x < 0 || y < 0 || x >= field.length || y >= field[0].length);

            if (field[x][y] == true) { // ellenőrzés
                System.out.println("Aknára léptél Egy életet elvesztettél");
                life--;
            } else {
                System.out.println("Siker! Túlélted");
                checkAround(field, x, y);
                counter++;
            }
        } while (life>0);
        System.out.println("Sajnos elfogyott az életed! Ennyi mezőt tippeltél sikeresen: " + (counter - 1));
    }

    public static void printField(boolean field[][]) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == true) {
                    System.out.print("X ");
                } else {
                    System.out.print("O ");
                }
            }
            System.out.print("\n");
        }
    }

    public static void checkAround(boolean[][] matrix, int x, int y) {
        int counter = 0;
        for (int j = y - 1; j <= y + 1; j++) {
            for (int i = x - 1; i <= x + 1; i++) {
                if (i >= 0 && i < matrix.length && j >= 0 && j < matrix[0].length) {
                    if (matrix[i][j] == true) { //ellenőrzés
                        counter++; // aknaszámolás
                    }
                }
            }
        }
        if (counter==1) System.out.println("Nincs para, csak 1 akna van körülötted!");
        else if (counter>1 && counter <5) System.out.println("Óvatosan kezd melegedni a helyzet!");
        else if (counter>4) System.out.println("Hurt Locker!");
        else System.out.println("Nincs akna körülötted!");
    }
    
}
