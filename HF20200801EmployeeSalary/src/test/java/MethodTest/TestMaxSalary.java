package MethodTest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import EmployeeBean.Employee;
import hu.braininghub.employeesalary.EmployeeService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author tbiro
 */
public class TestMaxSalary {
    EmployeeService es;
    
    public TestMaxSalary() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        es=new EmployeeService();
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void putNegativeNumbersToGetMaxSalary(){
        int sampleSalary1=-123244;
        int sampleSalary2=-2243244;
        
        List<Employee> testList = new ArrayList<>();
        Employee anna = new Employee("Anna",29,sampleSalary1);
        Employee regina = new Employee("Regina",29,sampleSalary2);  
        testList.add(anna);
        testList.add(regina);
        
        Employee results=es.getMaxSalary(testList);
        assertEquals(anna, results);
        }
    
        @Test
        public void putNullToGetMaxSalary(){
        Integer sampleSalary1=null;
        int sampleSalary2=-2243244;
        
        List<Employee> testList = new ArrayList<>();
        Employee anna = new Employee("Anna",29,sampleSalary1);
        Employee regina = new Employee("Regina",29,sampleSalary2);  
        testList.add(anna);
        testList.add(regina);
        
        Employee results=es.getMaxSalary(testList);
        assertEquals(regina, results);
        }
      
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}


}