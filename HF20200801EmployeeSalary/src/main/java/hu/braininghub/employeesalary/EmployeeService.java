/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employeesalary;

import EmployeeBean.Employee;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author tbiro
 */
public class EmployeeService {
    
    public static void main(String[] args) {

    List<Employee> employeeList = new ArrayList<>();
    employeeList.add(new Employee("Éva",19,89900));
    employeeList.add(new Employee("Panni",19,289900));
    employeeList.add(new Employee("Anna",29,123900));
    employeeList.add(new Employee("Regina",29,120900));
    employeeList.add(new Employee("Péter",39,199900));
    employeeList.add(new Employee("Aranka",39,299900));

    System.out.println("A legmagasabb fizetésű személy: "+getMaxSalary(employeeList));
    System.out.println("Életkor szerinti fizetés átlagok:");
    for (Map.Entry<Integer, Double> entry : getAvgSalaryByAge(employeeList).entrySet()) {
            System.out.println("Age: "+entry.getKey() + " - average salary: " + entry.getValue());
        }
    }

    public static Map<Integer,Double> getAvgSalaryByAge(List<Employee> employeeList) {
        Map<Integer,Double> avgSalaryByAge = new HashMap<>();
        for (Employee i:employeeList){
            int age= i.getAge();
            double salary=i.getSalary();
            if(avgSalaryByAge.get(age)==null) avgSalaryByAge.put(age,salary);
            else {
                double salatyToAdd=avgSalaryByAge.get(age)+salary;
                avgSalaryByAge.put(age,salatyToAdd/2); //egyelőre csak 2 taggal számolok, folyt köv-
            }
        }
        return avgSalaryByAge;
        
    }

    public static Employee getMaxSalary(List<Employee> employeeList) {
        Employee maxSalaryEmployee = Collections.max(employeeList);
        return maxSalaryEmployee;
        
    }
    
}