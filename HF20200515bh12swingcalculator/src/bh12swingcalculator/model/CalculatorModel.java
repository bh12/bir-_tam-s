package bh12swingcalculator.model;

public class CalculatorModel { //ebben tényleg csak getterek, setterek vannak...
    private int displayedNumber = 0;
    private int numberInMemory = 0;
    private Operation operation; //faszán enumokkal a lehetséges műveleteket

    public int getDisplayedNumber() {
        return displayedNumber;
    }

    public void setDisplayedNumber(int displayedNumber) {
        this.displayedNumber = displayedNumber;
    }

    public int getNumberInMemory() {
        return numberInMemory;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public void setNumberInMemory(int numberInMemory) {
        this.numberInMemory = numberInMemory;
    }
    
    
}
