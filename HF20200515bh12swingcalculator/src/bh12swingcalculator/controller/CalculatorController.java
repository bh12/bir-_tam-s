package bh12swingcalculator.controller;

import bh12swingcalculator.model.CalculatorModel;
import bh12swingcalculator.model.Operation;
import bh12swingcalculator.service.CalcService;
//import bh12swingcalculator.service.CalculatorService;
import bh12swingcalculator.view.CalculatorView;

public class CalculatorController implements CalcService {

    private CalculatorModel model;
    private CalculatorView view;
    //private CalculatorService calculatorService;//HF: kiszervezni egy interface moge es injektalni konstruktoron keresztul

    public CalculatorController() {
        this.model = new CalculatorModel(); //csak az alapadatok létehozása, pl műveletek, kijelzett érték, memória,
        this.view = new CalculatorView(this); //ez a konstruktor létrehozza a számológépet swingben. Ezt a thist továbbra sem értem!!
        //this.calculatorService = new CalculatorService();
        this.view.init();
    }

    public void handleClearButtonClick() {
        model.setDisplayedNumber(0);
        model.setNumberInMemory(0);
        model.setOperation(null);

        updateViewFromModel();
    }

    public void handleNumberButtonClick(String str) {
        String newButtonText = model.getDisplayedNumber() + str;
        model.setDisplayedNumber(Integer.valueOf(newButtonText));

        updateViewFromModel();
    }

    public void handlePlusButton() {
        this.model.setNumberInMemory(this.model.getDisplayedNumber());
        this.model.setOperation(Operation.ADDITION);
        this.model.setDisplayedNumber(0);

        updateViewFromModel();
    }

    public void handleEqualsButtion() {
        if (this.model.getOperation() != null) {
            switch (this.model.getOperation()) {
                case ADDITION:
                    int calcResult = addNumbers(this.model.getDisplayedNumber(), this.model.getNumberInMemory()); //ez volt: int calcResult =calculatorService.
                    this.model.setNumberInMemory(0);
                    this.model.setDisplayedNumber(calcResult);
                    break;
            }
            this.model.setOperation(null);
            updateViewFromModel();
        }
    }

    public void updateViewFromModel() {
        view.setScreenText(String.valueOf(model.getDisplayedNumber()));
    }

    @Override
    public int addNumbers(int n1, int n2) {
        return n1 + n2;    
    }

    @Override
    public int multipleNumbers(int n1, int n2) {
        return n1*n2;    }
}

/*
    DRY = Don't Repeat Yourself
*/