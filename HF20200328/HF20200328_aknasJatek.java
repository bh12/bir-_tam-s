/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200328_aknasjatek;

import java.util.Scanner;

public class HF20200328_aknasJatek {

    public static int FIELD_SIZE_A = 10, FIELD_SIZE_B = 12, NUMBER_OF_MINES = 5;
    public static int EMPTY_CHAR = 0, MINE_CHAR = 1, GATE_CHAR = 2, PLAYER_CHAR = 3;
    public static char EMPTY_SIGN = '0', GATE_SIGN = 'G', MINE_SIGN = 'X', PLAYER_SIGN = 'P';
    public static int XPOS, YPOS;
    public static int XMOVE, YMOVE;
    public static int COUNTER=0;
    public static final char UP = 'w', DOWN = 's', LEFT = 'a', RIGHT = 'd', EXIT = 'x';

    public static void main(String[] args) {
        int[][] field = new int[FIELD_SIZE_A][FIELD_SIZE_B];
        fillWithMines(field, NUMBER_OF_MINES);
        fillWithChar(field, GATE_CHAR);
        fillWithChar(field, PLAYER_CHAR);
        printField(field);
        move(field);
    }

    public static void fillWithMines(int[][] field, int mines) {
        for (int m = 1; m <= mines; m++) {
            fillWithChar(field, MINE_CHAR);
        }
    }

    public static void printField(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] == MINE_CHAR) {
                    System.out.print(MINE_SIGN);
                } else if (array[i][j] == GATE_CHAR) {
                    System.out.print(GATE_SIGN);
                } else if (array[i][j] == PLAYER_CHAR) {
                    System.out.print(PLAYER_SIGN);
                } else {
                    System.out.print(EMPTY_SIGN);
                }
                System.out.print(" ");
            }
            System.out.println("");
        }
    }

    public static void fillWithChar(int[][] field, int type) {
        int x, y;
        do {
            x = (int) (Math.random() * field[0].length);
            y = (int) (Math.random() * field.length);
        } while (field[y][x] == MINE_CHAR || field[y][x] == GATE_CHAR || field[y][x] == PLAYER_CHAR);
        field[y][x] = type;
        if (type == PLAYER_CHAR) {
            XPOS = x;
            YPOS = y;
        }
    }

    public static void move(int[][] field) {
        Scanner sc = new Scanner(System.in);
        XMOVE = 0;
        YMOVE = 0;
        System.out.println("Merre mozogjunk? " + UP + "=fel " + DOWN + "=le " + LEFT + "=bal " + RIGHT + "=jobb " + EXIT + "=kilép.");
        char input = sc.next().charAt(0);
        switch (input) {
            case UP:
                YMOVE = -1;
                break;
            case DOWN:
                YMOVE = 1;
                break;
            case LEFT:
                XMOVE = -1;
                break;
            case RIGHT:
                XMOVE = 1;
                break;
            case EXIT:
                return;
        }
        validateMove(field);
    }

    public static void validateMove(int[][] field) {
        if ((XPOS + XMOVE) < 0 || (XPOS + XMOVE) == field[0].length || (YPOS + YMOVE) < 0 || (YPOS + YMOVE) == field.length) {
            System.out.println("Fal!");
        } else {
            field[YPOS][XPOS] = EMPTY_CHAR;
            XPOS += XMOVE;
            YPOS += YMOVE;
            COUNTER++;
        }
        if (field[YPOS][XPOS] == GATE_CHAR) {
            winner();
        } else if (field[YPOS][XPOS] == MINE_CHAR) {
            lost();
        } else {
            field[YPOS][XPOS] = PLAYER_CHAR;
            printField(field);
            move(field);
        }
    }

    public static void lost() {
        System.out.println("Elvesztél");
        System.out.println((COUNTER-1)+" lépést tettél meg!");
    }

    public static void winner() {
        System.out.println("Kijutottál!");
                System.out.println(COUNTER+" lépést tettél meg!");
    }
}
