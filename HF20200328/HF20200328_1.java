/*
1. futóverseny eredményeinek feldolgozása: kérjük be a felhasználótól hány futó volt (legalább 3 ember kell a versenyhez), majd kérjük be a futók nevét és hogy hány perc alatt teljesítették a távot, majd a végén írjuk ki az első három helyezett futót és eredményeit.
 */
package hf20200328_1;

import java.util.Scanner;

public class HF20200328_1 {

    public static int MINIMAL_PARTICIPANTS = 3;
    public static int HOW_MANY_WINNERS_TO_LIST = 3;

    public static void main(String[] args) {
        String[][] participants = askParticipants();
        listOfWinners(HOW_MANY_WINNERS_TO_LIST, participants);
    }

    public static String[][] askParticipants() {
        Scanner sc = new Scanner(System.in);
        int participantNumber;
        do {
            System.out.println("Hány résztvevő van a versenyen?");
            participantNumber = sc.nextInt();
        } while (participantNumber < MINIMAL_PARTICIPANTS);
        String[][] participants = new String[participantNumber][2];
        for (int i = 0; i < participants.length; i++) {
            System.out.println("Add meg az " + (i + 1) + ". résztvevő nevét!");
            participants[i][0] = sc.next();
            System.out.println("Hány perc alatt teljesítette a távot?");
            participants[i][1] = sc.next();
        }
        return participants;
    }

    public static void listOfWinners(int HOW_MANY_WINNERS_TO_LIST, String[][] participants) {
        float[][] tempArray = new float[participants.length][2]; //segédtömb az eredményekkel és az eredeti helyezésekkel (float tömbben)
        for (int i = 0; i < participants.length; i++) {
            tempArray[i][0] = Float.parseFloat(participants[i][1]);//szövegből átalakítja float-tá
            tempArray[i][1] = i; //eltárolja az eredeti indexet, hogy később hivatkozni lehessen rá! Ez a kulcs.
        }
        for (int i = 0; i < participants.length; i++) { //indul a rendezés. Nem akartam az eredeti adatbázis tömbőt átrendezni
            for (int j = participants.length-1; j > i; j--) {
                if (tempArray[j][0] < tempArray[i][0]) {
                    float temp0 = tempArray[j][0];
                    float temp1 = tempArray[j][1];
                    tempArray[j][0] = tempArray[i][0];
                    tempArray[j][1] = tempArray[i][0];
                    tempArray[i][0] = temp0;
                    tempArray[i][1] = temp1;
                }
            }
        }
        System.out.println("A verseny győztesei:");
        for (int i=0;i<tempArray.length;i++){
            System.out.println((i+1)+".");
            int originalIndex=(int)(tempArray[i][1]); //tárolja az eredeti tömb indexét
            System.out.println("Név: "+participants[originalIndex][0]);
            System.out.println("Idő: "+participants[originalIndex][1]);
        }
    }

}
