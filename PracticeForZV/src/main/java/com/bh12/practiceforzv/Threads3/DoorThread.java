/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.practiceforzv.Threads3;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tbiro
 */
public class DoorThread extends Thread{
    private int howMany;

    public DoorThread(int howMany) {
        this.howMany = howMany;
    }
    @Override
    public void run(){
    for (int i=1;i<=howMany;i++){
        System.out.println("Csapkodom az ajót! "+i+".szor");
        if (i==5) try {
        while(!Thread.currentThread().isInterrupted()){
                
            synchronized (this){
                System.out.println("Várakozom!");
            wait();
            }
        }
        } catch (InterruptedException ex) {
            Logger.getLogger(DoorThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
    
}
