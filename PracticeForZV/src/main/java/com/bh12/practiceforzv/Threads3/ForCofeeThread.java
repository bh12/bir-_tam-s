/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.practiceforzv.Threads3;

/**
 *
 * @author tbiro
 */
public class ForCofeeThread extends Thread {

    private int howMany;

    public ForCofeeThread(int howMany) {
        this.howMany = howMany;
    }

    @Override
    public void run() {
        for (int i = 1; i <= howMany; i++) {
            System.out.println("Kávéért követelőzöm! " + i + ".szor");
        }
        System.out.println("feloldom a várakozást!");
        synchronized (this) {
            notifyAll();
        }
    }
}
