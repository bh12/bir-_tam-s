package clock;

public class Ora {

    private int hours;
    private int minutes;
    private int seconds;
    public String time;

    public String getClock() { // ez a getter
        makeString();
        return this.time;
    }

    public void setClock(int h, int m, int s) { // ez a setter
        this.hours = h;
        this.minutes = m;
        this.seconds = s;
    }

    public String makeString() { // a toString-re hibát írt ki, lehet, hogy foglalt?
        this.time = this.hours + ":" + this.minutes + "." + this.seconds; // itt készül a kimeneti változó
        return time;
    }

    public void plusMinutes(int m) {
        if (m > 60) {
            System.out.println("Egyelőre csak 60-nál kevesebb időt adj meg!");
            return;
        }
        this.minutes += m;
        if (this.minutes > 60) {
            this.hours++;
            this.minutes -= 60;
        }
    }
}
