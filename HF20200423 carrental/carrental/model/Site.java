
package carrental.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kopacsi
 */
public class Site {

    private int siteId;
    private String address;
    private List<Car> autok = new ArrayList<>(); //csak üres referencia //Car[] autok// a site-oknak van autójuk (benne vanalárendeltek neki)

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Car> getCars() {
        return autok;
    }

    public void setCars(List<Car> cars) { //új setter. Feltölti a car listát kocsikkal
        this.autok = cars;
    }

    @Override
    public String toString() {
        return "Site{" + "siteId=" + siteId + ", address=" + address + '}';
    }

}
