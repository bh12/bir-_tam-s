/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrental.model;

public abstract class Car {

    private String plateNumber;
    private String color;
    private String model;
    private int doorCount;
    private int kilometerCount;
    private int yearOfCreation;
    private int actualPetrolLevel;
    private int rentalPrice;
    private boolean isRented;

    public Car() { //üres konstruktor, de minek ez??
    }

    public void setRentalPrice(int rentalPrice) {
        this.rentalPrice = rentalPrice;
    }

    public int getRentalPrice() {
        return rentalPrice;
    }

    public Car(String plateNumber, String color, String model, int doorCount, int kilometerCount, int yearOfCreation) { //teljes konstruktor
        this.plateNumber = plateNumber;
        this.color = color;
        this.model = model;
        this.doorCount = doorCount;
        this.kilometerCount = kilometerCount;
        this.yearOfCreation = yearOfCreation;
        this.actualPetrolLevel=40; // egyelőre fixen megadom
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    public int getKilometerCount() {
        return kilometerCount;
    }

    public void setKilometerCount(int kilometerCount) {
        this.kilometerCount = kilometerCount;
    }

    public int getYearOfCreation() {
        return yearOfCreation;
    }

    public void setYearOfCreation(int yearOfCreation) {
        this.yearOfCreation = yearOfCreation;
    }

    public int getActualPetrolLevel() {
        return actualPetrolLevel;
    }

    public void setActualPetrolLevel(int actualPetrolLevel) {
        this.actualPetrolLevel = actualPetrolLevel;
    }

    public void setIsRented(boolean isRented) {
        this.isRented = isRented;
    }

    public boolean isIsRented() {
        return isRented;
    }
        

    @Override
    public String toString() {
        return "Car{" + "plateNumber=" + plateNumber + ", color=" + color + ", model=" + model + ", doorCount=" + doorCount + ", kilometerCount=" + kilometerCount + ", yearOfCreation=" + yearOfCreation + '}';
    }
    public abstract int kmCalc();
    public abstract double efficiencyRate();

   }
