/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrental.model;

/**
 *
 * @author tbiro
 */
public class ElectricCar extends Car{
    private double chargeLevel;

    public void setChargeLevel(double chargeLevel) {
        this.chargeLevel = chargeLevel;
    }

    public ElectricCar(double chargeLevel) {
        this.chargeLevel = chargeLevel;
    }

    @Override
    public int kmCalc() {
        return (int)(chargeLevel*efficiencyRate()*350);
    }

    @Override
    public double efficiencyRate() {
        double efficiencyRnd=Math.random()*0.4+0.6;
        return efficiencyRnd-super.getKilometerCount()/280000;
    }
    
    
}
