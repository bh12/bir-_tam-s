
package carrental.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kopacsi
 */
public class Company {

    private List<Site> sites = new ArrayList<>(); //olyan mint egy üres tömb: Site[] telepHelyek  //a cégben van benne a telephely. Ez egy lista, tartalmazz a telephelyeket

    public List<Site> getSites() {
        return sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

}
