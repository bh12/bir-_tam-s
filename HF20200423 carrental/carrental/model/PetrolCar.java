/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrental.model;

/**
 *
 * @author tbiro
 */
public class PetrolCar extends Car {
    
     @Override
    public int kmCalc() {
        return (int)(super.getActualPetrolLevel()*10*efficiencyRate());
    }

    @Override
    public double efficiencyRate() {
        double efficiencyRnd=Math.random()*0.4+0.6;
        return efficiencyRnd-super.getKilometerCount()/240000;
    }
}
