/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Car implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO) //ezzel elérjük, hogy ne nekünk kelljen az id-kat növelni
    private int carId;
    
    @Column 
    private String color;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="site_id") //ez megadja, hogy mi lesz a foreign key neve az adatbázisban
    private Site site;
    
    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    
    
    public int getCarId() {
        return carId;
    }

    public String getColor() {
        return color;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" + "carId=" + carId + ", color=" + color + ", site=" + site.toString() + '}';
    }
    
}
