/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.lotto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

/**
 *
 * @author tbiro
 */
public class Lotto implements QuickTip1 { //ezáltal az Interface által lesz a Lotto osztálynak egy "képessége"
    //lesz egy generateNumbers metódusa

    public static void main(String[] args) {
        Lotto lotto = new Lotto ();
        System.out.println("Lottószámok: " + lotto.generateNumbers (90, 5, 2));
    }

    @Override
    public HashMap<Integer, TreeSet> generateNumbers(int to, int type, int howMany) {
        HashMap<Integer, TreeSet> sheets = new HashMap<>();
        boolean wasCalculated;
        for (int nr = 1; nr <= howMany; nr++) {
            TreeSet<Integer> lotto = new TreeSet<>();
            for (int i = 1; i <= type; i++) {
                do {
                    int randomNr = (int) (Math.random() * to + 1);
                    wasCalculated = lotto.add(randomNr);
                } while (wasCalculated == false);
            }
            sheets.put(nr, lotto);
        }
        return sheets;
    }
}
