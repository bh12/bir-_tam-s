/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.lotto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

/**
 *
 * @author tbiro
 */
public interface QuickTip1 {
    public HashMap<Integer,TreeSet> generateNumbers(int to, int type, int howMany);
}
