/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingbookreader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tbiro
 */
public class Serialize {
    private String link;
    private File file;
    private String textToBook;

    public Serialize(String link, String textToBook) {
        this.link=link;
        this.file = new File(link);
        serializeToFile(file, textToBook);
    }
    
   public void serializeToFile (File fileName, String toWrite){
    try (FileOutputStream fos = new FileOutputStream(fileName);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            Book toSer = new Book("toSer");
            toSer.setText(toWrite);
            oos.writeObject(toSer);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        } 

}
}