/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingbookreader;

import java.io.Serializable;

/**
 *
 * @author tbiro
 */
public class Book implements Serializable {
    private final static long serialVersionUID = 2L;
    private String title;
    private String text;

    public Book(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
    
    public void toSerialise (Book book){
        
    }
    
    @Override
    public String toString() {
        return "Book{" + "title=" + title + ", text=" + text + '}';
    }
    
    
}
