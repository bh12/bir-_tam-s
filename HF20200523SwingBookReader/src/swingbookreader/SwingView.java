
package swingbookreader;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

    public class SwingView extends JFrame {
    
    
    private JTextArea textArea;
    private JButton buttonSave;
    private JButton serialize;
    
    public JTextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(JTextArea textArea) {
        this.textArea = textArea;
    }

    public SwingView(Book book) { //ezzel a konstruktorral hozzuk létre a view-n beül majd a controllert! Vagyis majd ez kell a konstruktorba: SwingView(Controller controller)
        
        textArea=new JTextArea("Book text:");
        buttonSave= new JButton ("Save");
        serialize= new JButton ("Serialize");
        init();
        readFromBook(book);
    }
    
    public void init(){
        setSize(500,500);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(textArea);
        add(buttonSave);
        add(serialize);
        setTitle("Swing Window");
        setLayout(new GridLayout(3,1)); //majd gridBagLayout-ra átdolgozni
        buttonSave.addActionListener(ifPressed -> { 
                            WriteTofile toFile = new WriteTofile("write.txt", textArea.getText());
        });
        serialize.addActionListener(ifPressed -> {
                            Serialize toSer = new Serialize("BookToSerialize.ser",textArea.getText()); 
        });  
    }
    
    public void readFromBook (Book book){
        textArea.setText(book.getText()); //book.getText()
    }
    
    /*
        public void modifyView (String text){
            textArea.append("\n"+text);
        }
    */
    
    }