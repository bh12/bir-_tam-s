/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200515swingwithmvc.Controller;

import hf20200515swingwithmvc.Model.Model;
import hf20200515swingwithmvc.View.SwingView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.time.LocalDate;
import static java.time.LocalDate.now;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tbiro
 */
public class Controller implements WriteToFile {
    private Model model;

    public Controller(Model model) {
        this.model = model;
    }
    
    public void logging(SwingView pressed){ // ez csak loggoljon!!!
        this.model.setLogCounter(this.model.getLogCounter()+1); //növeli 1-gyel a countert
        
        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        System.out.println("Time: "+time);
                
        StringBuilder sb = new StringBuilder();
        sb.append(this.model.getLogCounter());
        sb.append(". [");
        sb.append(date);
        sb.append(" ");
        sb.append(time);
        sb.append("] :Button pressed");
        this.model.setLogMap(time, sb.toString()); //berakja a model-be egy listába a stringet
        pressed.modifyView(sb.toString()); //vissza a view-ba és ott írja ki
        writeLogs(model.getFile(),model.getLogMap()); //meghívja a writeToFile metódust
    }
            
    @Override
    public void writeLogs(File file, Map<LocalTime,String> logMap) { //csak azt kellene ide kifejteni, ami az interface alkalmazása során extra az interfacehez képest
        try  (FileWriter fw=new FileWriter(file);
            BufferedWriter bw=new BufferedWriter(fw)) {
            
            int previousHour=0;
            for (Map.Entry<LocalTime,String> entry: logMap.entrySet()) { //Így lehet végigmenni a map-en!!
                int hour= entry.getKey().getHour();
                if (previousHour!=hour) bw.write(System.lineSeparator());
                previousHour=hour;
                bw.write(entry.getValue());
                bw.write(System.lineSeparator());
            }
        //bw.close(); //a try-with resources lezárja, ha jól tudom így ez nem kell
    }   catch (IOException ex) {
            System.out.println("Error during writing to File");}
}
    //ide kerülnek azok a metódusok, amelyek a működéshez kellenek.
    //pl. ha megnyomja a gombot, akkor itt kezeljük le hogy mi történjen:
    // adunk egy sorszámot, majd a dátumot, időt lekérni, majd visszaküldeni (metódusból) és ott beletenni JTextArea-ba.
    //ezután lehetne a file-ba írás egy interface-ben, hátha máshol is kellhet ez a szolgáltatás
}
