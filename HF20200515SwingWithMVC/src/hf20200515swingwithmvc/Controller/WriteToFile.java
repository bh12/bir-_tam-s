/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200515swingwithmvc.Controller;

import java.io.File;
import java.io.FileWriter;
import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

public interface WriteToFile {
    public void writeLogs (File file, Map<LocalTime,String> logMap);           
}
    
