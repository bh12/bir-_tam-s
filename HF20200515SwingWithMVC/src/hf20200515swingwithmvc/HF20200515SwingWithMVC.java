/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200515swingwithmvc;

import hf20200515swingwithmvc.Controller.Controller;
import hf20200515swingwithmvc.Model.Model;
import hf20200515swingwithmvc.View.SwingView;

/**
 *
 * @author tbiro
 */
public class HF20200515SwingWithMVC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // A main csak indítja az alkalmazást
        //pl. létrehozunk egy Swing ablakot, vagy init()-tel indítjuk el.
        //innen majd a controllert kell indítani!
        Model model = new Model();
        Controller controller = new Controller(model); //elkészül egy példány a contollerból is, de ezt innen kell egyáltalán?
        SwingView view = new SwingView(controller); //egyelőre itt létreozom a nézet osztályt, majd init() metódusával elindítom  
        view.init(); //indul a buli!
    }
    
}
