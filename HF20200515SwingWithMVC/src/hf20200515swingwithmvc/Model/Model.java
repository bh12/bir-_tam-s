/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200515swingwithmvc.Model;

import java.io.File;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class Model {
    int logCounter; //ez tárolja, hogy hanyadik log
    //private List<String> logLines =new ArrayList<>(); //ebbe tesszük majd bele a logokat, gombnyomás esetén
    private File file =new File("logs.txt"); //ide mentegetjük soronként a logokat
    private Map<LocalTime,String> logMap = new TreeMap<>();  //lehet map-be is tenni így: 

    public int getLogCounter() {
        return logCounter;
    }

    public Map<LocalTime, String> getLogMap() {
        return logMap;
    }

    public File getFile() {
        return file;
    }

    public void setLogCounter(int logCounter) {
        this.logCounter = logCounter;
    }

    public void setLogMap(LocalTime time,String logLine) {
        this.logMap.put(time, logLine);
    }
    
}
