/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200514swingfromfile;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;


public class HF20200514SwingfromFile {

    
    public static void main(String[] args) {
       MainFrame2 frame2=new MainFrame2("Házi");

        JFileChooser fileOpener = new JFileChooser("C:\\Brainhub\\Swing");
        int returnValue = fileOpener.showOpenDialog(null);
        File selectedFile=fileOpener.getSelectedFile();
        ArrayList<String> lines = new ArrayList<>();
        
        try (FileReader fr= new FileReader(selectedFile);
                BufferedReader br= new BufferedReader(fr)){
       
        String line=br.readLine();
        while (line!=null){
            lines.add(line);
            line=br.readLine();
        }
        frame2.init(lines);
    }   catch (IOException ex) {
            System.out.println("Hib a file elérése közben!");
        }
    
}
}

    

