package hf20200514swingfromfile;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.border.Border;

/**
 *
 * @author tbiro
 */
public class MainFrame2 extends JFrame {

    private JLabel label;

    public MainFrame2(String title) throws HeadlessException {
        super(title);
        //label = new JLabel("címke");
    }

    public void init(ArrayList<String> line) {
        setVisible(true); //láthatóság
        setSize(600, 500);//ablakméret
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//a leállításhoz kell...
        setLayout(new FlowLayout(250, 100, 100));

        for (String sr : line) {
            label = new JLabel(sr);
            label.setVerticalAlignment(JLabel.TOP);
            label.setFont(new Font("Helvetica", Font.PLAIN, 25));
            label.setPreferredSize(new Dimension(150, 50));
            Border border = BorderFactory.createLineBorder(Color.ORANGE);
            label.setBorder(border);
            add(label);
        }

    }

}
