/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zhfeladataruhaz;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ZHfeladatAruhaz {

    public static List<Products> stockRoom = new ArrayList<>(); //itt a Products ősosztály és ide elég ennyi
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        String input = "";
        loadtTempProducts();
        while (!input.equals("EXIT")) {
            System.out.println("Válassz a menüből:");
            System.out.println("ADD, REMOVE, REPORT, EXIT");
            input = sc.next();
            switch (input) { //ezt majd ciklussá kell tenni
                case "ADD":
                    addProduct();
                    break;
                case "REMOVE":
                    remove();
                    break;
                case "REPORT":
                    report();
                    break;
                case "EXIT":
                    System.out.println("goodByte!");
                    break;
            }
        }
    }

    public static void addProduct() {
        System.out.println("Add meg a vonalkódot?");
        int barCode = sc.nextInt();
        System.out.println("Milyen típusú (cheap,average,expensive,luxury,popular)");
        String type = sc.next();
        Properties whichEnum;
        switch (type) { //biztosan meg lehet oldani szebben is, de akkör figyelni kell, 
            case "cheap":
                whichEnum = Properties.cheap;
                break;
            case "average":
                whichEnum = Properties.average;
                break;
            case "expensive":
                whichEnum = Properties.expensive;
                break;
            case "luxury":
                whichEnum = Properties.luxury;
                break;
            case "popular":
                whichEnum = Properties.popular;
                break;
            default:
                whichEnum = null;
        }
        System.out.println("Gyártója");
        String manufacturer = sc.next();
        System.out.println("Speciális csoportja? (K=KitchenTools, E=Enttertainment, B=Beauty");
        String whatKindOf = sc.next();
        System.out.println("Ára?");
        int price = sc.nextInt();
        Products tool = null;
        switch (whatKindOf) {
            case "K":
                tool = new KitchenTools(whichEnum, manufacturer, barCode, price); //enum-ot oóhogyan tudok beállítani?
                break;
            case "E":
                tool = new Entertainment(whichEnum, manufacturer, barCode, price); //enum-ot oóhogyan tudok beállítani?
                break;
            case "B":
                tool = new Beauty(whichEnum, manufacturer, barCode, price); //enum-ot oóhogyan tudok beállítani?
                break;
        }
        stockRoom.add(tool);
    }

    public static void remove() {
        System.out.println("Melyik vonalkódú terméket távolítsuk el?");
        int barCode = sc.nextInt();
        Iterator itr = stockRoom.iterator();
        while (itr.hasNext()) {
            Products x = (Products) itr.next();
            if (x.getBarCode() == barCode) {
                itr.remove();
            }
        }
    }

    public static void report() {
        System.out.println("Hány termék van: ");
        System.out.println(stockRoom
                .stream()
                .count());
        System.out.println("Hány olyan szórakoztató eszköz van, ami még legalább 2 bekapcsolást bír?");
        int counter = 0;
        Entertainment temp;
        for (Products i : stockRoom) {
            if (i instanceof Entertainment) {
                temp = (Entertainment) i;
                if (5 - temp.getSwithes() >= 2) {
                    counter++;
                }
            }
        }
        System.out.println(counter);
        /*
        System.out.println(stockRoom
                        .stream()
                        .filter(f -> f.getClass().equals(Entertainment.class))                  
                        .filter(f-> (SwitchAble)f.getSwithes()>2));
         */
        ///storage.products.stream()
        //.filter(p -> p.equals(Fun.class))
        //.filter{t->(interface)x = t; x.swicthcount>2)
    }

    public static void loadtTempProducts() {
        KitchenTools k = new KitchenTools(Properties.cheap, "Alkon", 1234, 254);
        Entertainment e = new Entertainment(Properties.expensive, "mama", 2345, 789);
        e.setSwithes(5);
        try {
            e.switchOn();
        } catch (SpecialFauilureEx ex) {
            System.out.println(ex.getMessage());
        }
        Entertainment ee = new Entertainment(Properties.popular, "papa", 4567, 800);
        ee.setSwithes(1);
        Beauty b = new Beauty(Properties.luxury, "apa", 3456, 895);
        stockRoom.add(k);
        stockRoom.add(e);
        stockRoom.add(ee);
        stockRoom.add(b);

    }
}
