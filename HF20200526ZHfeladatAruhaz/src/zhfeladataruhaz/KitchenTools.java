/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zhfeladataruhaz;

/**
 *
 * @author tbiro
 */
public class KitchenTools extends Products implements LiftAble,SwitchAble {
    
   private int swithes;
    
    public KitchenTools(Enum properties, String manufacturer, int barCode, int price) {
        super(properties, manufacturer, barCode, price);
        this.swithes=0;
    }
    public KitchenTools(){
}

    public int getSwithes() {
        return swithes;
    }

    public void setSwithes(int swithes) {
        this.swithes = swithes;
    }

    @Override
    public void switchOn() {
     this.swithes++;
    }
}
