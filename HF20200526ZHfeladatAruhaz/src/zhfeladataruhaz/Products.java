/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zhfeladataruhaz;

import java.time.LocalDate;

/**
 *
 * @author tbiro
 */
public abstract class Products implements Comparable<Products> {
    private Enum properties;
    private String manufacturer;
    private int barCode;
    private int price;
    private LocalDate regDate;

    public int getBarCode() {
        return barCode;
    }
    public Products (){
        
    }

    @Override
    public String toString() {
        return getClass()+"Products{" + "properties=" + properties + ", manufacturer=" + manufacturer + ", barCode=" + barCode + ", price=" + price + ", regDate=" + regDate + '}';
    }
    
    public Products(Enum properties, String manufacturer, int barCode, int price) {
        this.properties = properties;
        this.manufacturer = manufacturer;
        this.barCode = barCode;
        this.price = price;
    }

    public void setProperties(Enum properties) {
        this.properties = properties;
    }

    
    @Override
    public int compareTo(Products o) {
       if (this.barCode>o.barCode) return 1;
       else if (this.barCode<o.barCode) return -1;
       else return 0;
    }
    
    
}
