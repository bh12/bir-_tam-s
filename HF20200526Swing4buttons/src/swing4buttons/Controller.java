/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing4buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author tbiro
 */
public class Controller {
     private Model model; //controller irányít, Ő a főnök. Ő indul el először
     private SwingView view;

    public Controller() {
        this.model = new Model();
        this.view= new SwingView(this); 
    }
    
    public void logging(Employee emp) {
        this.model.setLogCounter(this.model.getLogCounter() + 1); //ehetne egy metódus us
        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        StringBuilder sb = new StringBuilder();
        sb.append(this.model.getLogCounter());
        sb.append(". [");
        sb.append(date);
        sb.append(" ");
        sb.append(time.format(DateTimeFormatter.ofPattern("HH:mm.ss")));
        sb.append("] :Button pressed"); //eddig készül a logString
        sb.append(" - New "+emp+" object added.");

        model.setLogMap(model.getLogCounter(), time); //berakja a model-be egy Map-ba a stringet
        model.addToList(emp);
        view.modifyView(sb.toString()); //vissza a view-ba és ott írja ki

    }
    
    public void serialize () {
    try (FileOutputStream fos = new FileOutputStream("output.ser"); //try-with res.
            ObjectOutputStream oos = new ObjectOutputStream(fos)){ 
            
            oos.writeObject(model.getList());
        } 
        catch (IOException ioe) 
        {
            System.out.println("Hiba a file írása közben!"); 
            ioe.printStackTrace();
        }
    }
    public void readFromFile () {
     List<Employee> fromFile = new ArrayList<>();
     try  (FileInputStream fis = new FileInputStream("output.ser");
            ObjectInputStream ois = new ObjectInputStream(fis)){
            fromFile = (ArrayList) ois.readObject();
            model.setList(fromFile);
            for (Employee emp: fromFile){
                System.out.println(emp);
            }
        } 
        catch (IOException ioe) {
            System.out.println("Hiba a file olvasása közben!");
            ioe.printStackTrace();
            return;
        } 
        catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
}
}
    