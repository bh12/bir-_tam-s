/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing4buttons;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author tbiro
 */
public class SwingView extends JFrame {
    
    private Controller controller; 
    private JTextArea textArea;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;

    public JTextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(JTextArea textArea) {
        this.textArea = textArea;
    }
    
    public SwingView(Controller controller) { //ezzel a konstruktorral hozzuk létre a view-n beül majd a controllert! Vagyis majd ez kell a konstruktorba: SwingView(Controller controller)
        this.controller=controller; 
        textArea=new JTextArea("Action Event Logs:");
        button1= new JButton ("make a boss");
        button2= new JButton ("make a worker");
        button3= new JButton ("Serialize");
        button4= new JButton ("Open .ser file");
        init();
    }
    
    public void init(){
            setSize(500,500);
            setVisible(true);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            add(textArea);
            add(button1);
            add(button2);
            add(button3);
            add(button4);
            setTitle("Swing Window");
            setLayout(new GridLayout(5, 4));
            button1.addActionListener(ifPressed -> {
                Boss boss = new Boss("Pali");
                controller.logging(boss);  
            });
            button2.addActionListener(ifPressed -> {
                Worker worker = new Worker("Ödön");
                controller.logging(worker);
            });
            
            button3.addActionListener(x->controller.serialize());
            button4.addActionListener(x->controller.readFromFile());
    }
    
        public void modifyView (String text){
            textArea.append("\n"+text);
        }
        
    
}
