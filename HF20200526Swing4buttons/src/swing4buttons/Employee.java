package swing4buttons;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tbiro
 */
public abstract class Employee implements Serializable{
     public String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return  getClass() +" class"; 
    }
     
}
