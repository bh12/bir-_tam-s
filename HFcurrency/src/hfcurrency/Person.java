/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hfcurrency;


public class Person {
    private String name;
    private Currency type;
    private int howMuchMoney;
    private int age;

    public Person(Currency currencyType, int howMuchMoney, int age, String name) {
        this.type = currencyType;
        this.howMuchMoney = howMuchMoney;
        this.age = age;
        this.name= name;
    }
    public Person () {
        //megy üresen is
    }
    

    public void setName(String name) {
        this.name = name;
    }

    public void setType(Currency type) {
        this.type = type;
    }

    public void setHowMuchMoney(int howMuchMoney) {
        this.howMuchMoney = howMuchMoney;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Currency getType() {
        return type;
    }

    public int getHowMuchMoney() {
        return howMuchMoney;
    }

    public int getAge() {
        return age;
    }
 
    }
    
    

