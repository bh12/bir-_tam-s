/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hfcurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class HFcurrency {
    
public static int FROM_AGE=18;
public static int TO_AGE=49;

    public static void main(String[] args) {
        
        List<Person> personsList = new ArrayList<>();
        Person temp = new Person();
        for (int i = 0; i < 5; i++) {
            temp.setAge((int) (Math.random() * 100 + 1));
            temp.setHowMuchMoney((int) (Math.random() * 1000 + 1));
            temp.setName("Személy" + i);
            int rnd = (int)(Math.random()*5+1);
            switch (rnd){
                case 1:
                    temp.setType(Currency.GBP);
                    break;
                case 2:
                    temp.setType(Currency.EUR);
                    break;
                case 3:
                     temp.setType(Currency.JPN);
                     break;
                default:
                    temp.setType(Currency.HUF);      
        }      
            personsList.add(temp);
        }
        /*int sample=personsList.get(0).getHowMuchMoney();
        System.out.println(sample);
        */ 
        System.out.println("1.feladat. Az emberek vagyonánnak összértéke HUF-ban: "+CountMoney(personsList));
        System.out.println("2.feladat. A vagyonok átlaga: "+(double)CountMoney(personsList)/personsList.size());
        System.out.println("3.feladat: A vagyonok átlaga "+FROM_AGE+" és "+TO_AGE+" életkor között:"+CountMoney(personsList, FROM_AGE, TO_AGE)/personsList.size());
        System.out.println("4.feladat: Ez a Medián nekem nem érthető!");
        Set<Person> orderedList = new TreeSet<>(personsList);
        for (int i = 0; i < orderedList.size();i++){
            //System.out.println(orderedList.);//nem  megy a kilistázás
        }
    }

    public static int CountMoney(List<Person> personsList, int fromAge, int toAge) {
        int moneyCount=0;
        for (int i = 0; i < personsList.size();i++){
            if (personsList.get(i).getAge()>=fromAge && personsList.get(i).getAge()<=toAge) {
            moneyCount+=personsList.get(i).getHowMuchMoney()*personsList.get(i).getType().getRate();
            }
        }
        return moneyCount;
    }
    public static int CountMoney(List<Person> personsList) {
        int moneyCount=0;
        for (int i = 0; i < personsList.size();i++){
           
            moneyCount+=personsList.get(i).getHowMuchMoney()*personsList.get(i).getType().getRate();
            
        }
        return moneyCount;
    }
}
