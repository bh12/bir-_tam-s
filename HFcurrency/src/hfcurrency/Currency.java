/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hfcurrency;


public enum Currency {
    HUF(1),
    EUR(270),
    GBP(315),
    USD(290),
    JPN(99),
    RMB(74);
    
private final int rate;

    private Currency(int rate) {
        this.rate = rate;
    }

    public int getRate() {
        return rate;
    }


}
