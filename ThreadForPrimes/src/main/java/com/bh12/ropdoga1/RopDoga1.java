package com.bh12.ropdoga1;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
1. Írj programot, ami egyszerre N szálon egy meghatározott intervallumban lévő prím számokat kigyűjti egy közös kollekcióba. 
Az intervallum legyen egyenlően elosztva a szálak között, és maga az intervallum is legyen paraméterezhető. 
A program futása végén írjuk ki a kollekció elemeit. A kiírás történhet fájlba illetve konzolra is, és ez a számító osztály létrejöttekor dőljön el (a kiíró metódus mindenképp abstract legyen).
 */
public class RopDoga1 {
    public static final int NUMBER_OF_THREADS=5;
    public static final int PRIMES_FROM=3;
    public static final int PRIMES_TO=1000;
    
    public static void main(String[] args){
        ExecutorService es = Executors.newFixedThreadPool(NUMBER_OF_THREADS); 
        Calculator calculator = new Calculator();
        es.submit(calculator); //ez végzi több szálon a pírmkeresést és ír listába
        es.shutdown(); //ez meg leállítja
    }
}
