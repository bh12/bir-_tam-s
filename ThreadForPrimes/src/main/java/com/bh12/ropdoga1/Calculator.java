/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.ropdoga1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import java.util.List;

public class Calculator implements Runnable {

    private List<Integer> primes;
    private boolean isPrime;
    public static final int PRIMES_FROM = 5;
    public static final int PRIMES_TO = 1000;

    public List<Integer> getPrimes() {
        return primes;
    }

    public void setPrimes(List<Integer> primes) {
        this.primes = primes;
    }

    @Override
    public String toString() {
        return "Calculator{" + "primes=" + primes + '}';
    }

    @Override
    public void run() {
        System.out.println("Indul a szál!");
        for (int i = PRIMES_FROM; i <= PRIMES_TO; i++) {
            isPrime = TRUE;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = FALSE;
                    System.out.println(i+" Nem prím!");
                } 
            }
            if (isPrime == TRUE) {
                primes.add(i);
            }
        }
        //kiírás konzolra:
        System.out.println("A prímszámok a következőek:");
        for (Integer i : primes) {
            System.out.println(i + ",");
        }
        //kírom file-ba is:
        File file = new File("primes.txt");

        try (FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);) {

            StringBuilder sb = new StringBuilder();
            for (Integer e : primes) {
                sb.append(e.toString());
                sb.append(System.lineSeparator());
            }
            bw.write(sb.toString());
        } catch (IOException e) {
            System.out.println("Hiba a fájl írása közben! :(");
        }
    }

}
