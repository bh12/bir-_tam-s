/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.javaeeperson5;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tbiro
 */
@Singleton
public class PersonDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void newPerson (String name, int age, String email){
        Person person = new Person (name,age,email);
        em.persist(person);
    }
}
