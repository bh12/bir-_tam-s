/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.javaeeperson5;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author tbiro
 */
@Singleton
public class PersonService {
    @EJB
    PersonDao persoDao;
    
    public boolean emailCheck (String email){
        return email.contains("@");
    }
    public void persistNewPerson (String name, int age, String email){
        persoDao.newPerson(name, age, email);
    }
}
