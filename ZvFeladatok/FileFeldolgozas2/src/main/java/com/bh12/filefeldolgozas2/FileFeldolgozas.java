/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.filefeldolgozas2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.System.lineSeparator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tbiro
 */
public class FileFeldolgozas {
    public static void main(String[] args) {
        openFile("pelda.txt");
    }

    public static void openFile(String fileName) {
        try (FileReader fr = new FileReader(fileName);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            List<Fruit> fruits = new ArrayList<>();
            while (line !=null) {
                
                String[] split = line.split(";");
                
                String name=split[0];
                int kG=parseInt(split[1]);
                int price=parseInt(split[2]);
                
                Fruit fruit = new Fruit(name,kG,price);
                fruits.add(fruit);
                fruit.getResult(kG, price); //ha lesz majd egy kis időm, megoldom lamdával is! :-)
                
                StringBuilder sb = new StringBuilder();
                sb.append(fruit.getName());
                sb.append(";");
                sb.append(fruit.getTotalPrice());
                sb.append(lineSeparator());
                String resultString = sb.toString();
                writeToFile (resultString);
                line = br.readLine();
            }

        } catch (IOException ex) {
            System.out.println("Hiba a file olvasásakor!");
        } 
    }

public static void writeToFile (String toWrite){
    try (FileWriter fw = new FileWriter ("result.txt",true);
        BufferedWriter bw = new BufferedWriter(fw);){
        
        bw.write(toWrite);
    }   catch (IOException ex) {
            System.out.println("Hiba a file írásakor!");
        }
}
}
