/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.filefeldolgozas2;

/**
 *
 * @author tbiro
 */
public class Fruit implements CalcResult {
    private String name;
    private int kG;
    private int kgPrice;
    private int totalPrice;

    public Fruit(String name, int kG, int kgPrice) {
        this.name = name;
        this.kG = kG;
        this.kgPrice = kgPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getkG() {
        return kG;
    }

    public void setkG(int kG) {
        this.kG = kG;
    }

    public int getKgPrice() {
        return kgPrice;
    }

    public void setKgPrice(int kgPrice) {
        this.kgPrice = kgPrice;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }


    @Override
    public void getResult(int kG,int price) {
        this.totalPrice=kG*price;
    }
}
