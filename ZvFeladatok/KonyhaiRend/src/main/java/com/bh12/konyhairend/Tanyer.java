/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.konyhairend;

/**
 *
 * @author tbiro
 */
public class Tanyer extends KonyhaiEszkoz{

    private boolean isDirty;

    public Tanyer(boolean isDirty) {
        this.isDirty = isDirty;
    }

    public boolean isIsDirty() {
        return isDirty;
    }
    
    public void setIsDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }
    
    
    @Override
    public boolean isDirty() {
            return this.isIsDirty();
        }
}
