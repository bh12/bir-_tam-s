/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.konyhairend;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tbiro
 */
public class Main {

    public static void main(String[] args) {

        //feltöltés
        boolean dirty;
        double dirtyRandom;
        int typeRandom;
        ArrayList<KonyhaiEszkoz> konyhaPult = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            dirtyRandom = Math.random();
            typeRandom = (int) (Math.random() * 100 + 1);
            if (dirtyRandom < 0.5) {
                dirty = true;
            } else {
                dirty = false;
            }
            if (typeRandom < 30) {
                konyhaPult.add(new Tanyer(dirty));
            } else {
                konyhaPult.add(new Pohar(dirty));
            }
            System.out.println(i + ".eszköz " + konyhaPult.get(i).isDirty());
        }
        //singleton Andras
        Andras andras = Andras.getInstance();
        ArrayList<KonyhaiEszkoz> mosogato;
        try {
            mosogato = andras.checkDirtyAndPut(konyhaPult);
        } catch (SpecialException se) {
            System.out.println(se.getMessage());
        }

    }
}
