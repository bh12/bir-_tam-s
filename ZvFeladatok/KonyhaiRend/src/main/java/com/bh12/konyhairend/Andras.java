/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.konyhairend;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author tbiro
 */
public class Andras {

    private static Andras INSTANCE;

    public ArrayList<KonyhaiEszkoz> checkDirtyAndPut(ArrayList<KonyhaiEszkoz> konyhaPult) throws SpecialException {
        //itt fogom kifejteni mit csinál András
        ArrayList<KonyhaiEszkoz> mosogato = new ArrayList<>();

        Iterator<KonyhaiEszkoz> i = konyhaPult.iterator();
        while (i.hasNext()) {
            KonyhaiEszkoz ke = i.next();
            if (ke.isDirty() == true) {
                mosogato.add(ke);
                i.remove();
                if (mosogato.size() == 15) {
                    throw new SpecialException("Annyira sok a koszos cucc, hogy meghívom Hrenkó Tamás Singleton-t, hogy dobja tovább az exception-t a BH12 trehány hallgatókank!");
                    //ekkor leáll a program futása
                }
            }
        }
        return mosogato;
    }

    public static Andras getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Andras();
        }
        return INSTANCE;
    }
}
