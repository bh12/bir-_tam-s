/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.ropdoga2;

import com.sun.corba.se.impl.orbutil.ObjectWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 2. Írj programot, ami listában tárolt Employee(név, kor, telefonszám, lakcím)
 * típusú objektumokat kiír egy fájlba.
 */
public class RopDoga2 {

    public static void main(String[] args) throws IOException {
        Employee szemely = new Employee("Jóska", 23, "06-52-256-563", "Farhátasdűlő");
        Employee szemely2 = new Employee("Pista", 63, "06-52-256-568", "Almaháza");
        ArrayList<Employee> personList = new ArrayList<>();
        personList.add(szemely);
        personList.add(szemely2);
        writeToFile(personList);
        serialise(personList);
    }

    public static void writeToFile(ArrayList<Employee> list) {
        File employees = new File("employees.txt");

        try (FileWriter fw = new FileWriter(employees);
                BufferedWriter bw = new BufferedWriter(fw);) {

            StringBuilder sb = new StringBuilder();
            for (Employee e : list) {
                sb.append(e.toString());
                sb.append(System.lineSeparator());
            }
            bw.write(sb.toString());
        } catch (IOException e) {
            System.out.println("Hiba a fájl írása közben! :(");
        }
    }

    public static void serialise(ArrayList<Employee> list) throws FileNotFoundException {
        File file = new File("serialise.ser");

        try (FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);) {
            oos.writeObject(list);

        } catch (IOException e) {
            System.out.println("Hiba a fájl írása közben! :(");
        }

    }
}
