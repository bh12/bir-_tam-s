/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.calculator;

/**
 *
 * @author tbiro
 */
public class CalcFactory {

    private CalcType type;

    public CalcType getType() {
        return type;
    }

    public void setType(CalcType type) {
        this.type = type;
    }

    public CalcFactory(CalcType type) {
        this.type = type;
    }

    public static Calculator getCalculator(CalcType type) { //ez a Factory pattern
        Calculator calc;
        if (type.equals(CalcType.SientificCalculator)) {
            return new ScientificCalculator();
        } else {
            return new SpecialCalculator();
        }
    }
}
