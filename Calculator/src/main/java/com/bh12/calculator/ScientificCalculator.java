/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.calculator;

/**
 *
 * @author tbiro
 */
public class ScientificCalculator extends Calculator{
    
    private double c; //erre a hatványra emeli

    public double getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }
    
    public double power (double input, double c){
        return Math.pow(input,c);
    }
}
