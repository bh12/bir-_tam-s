/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.calculator;

/**
 *
 * @author tbiro
 */
public class Calculate {
    public static void main(String[] args) {
        System.out.println("Na milyen calculator legyen?");
        
        CalcType type=CalcType.SientificCalculator;
        //Itt a factory pattern
        Calculator calc = CalcFactory.getCalculator(type);
        
        System.out.println("Két szám összeadása: "+calc.sum(4, 6));
        
        //mivel a spekcó kalkulátorok többet tudnak, ezért kasztlással tudnám megoldani a többlet műveletre felkészítést
        ScientificCalculator scientCalc = (ScientificCalculator) calc;
        System.out.println("Két szám scientific kalkulátorral történő hatványozása: "+scientCalc.power(5,6));
        
        //itt az a nehéz, hogy ha leszármazott példány lesz a calc-ból, az csak rá jellemző metódust hívna

    }
    
}
