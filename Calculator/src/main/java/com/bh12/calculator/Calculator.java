/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.calculator;

/**
 *
 * @author tbiro
 */
public abstract class Calculator {
    private double a;
    private double b;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }
    
    
    public double sum (double a, double b){
        return a+b;
    }
    public double minus (double a, double b){
        return a-b;
    }
    public double division (double a, double b){
        if (b==0) {
            throw new ArithmeticException(); //ezt majd el kell kapni valahol
        }
        return a/b;
    }
    public double multiplic (double a, double b) {
        return a*b;
    }
}
