/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import com.bh12.prezhfeladat4.StudentDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tbiro
 */
@WebServlet(name = "ResultServlet", urlPatterns = {"/ResultServlet"})
public class ResultServlet extends HttpServlet {

    @EJB
    StudentDAO strudentDao;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResultServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResultServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

  
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String name = (String)req.getAttribute("name");
        int firstZh= Integer.parseInt(req.getParameter("firstZh"));
        int secondZh= Integer.parseInt(req.getParameter("secondZh"));
        int zV= Integer.parseInt(req.getParameter("zV"));
        
//        if (name.equals(null) || firstZh==0 || )secondZh==0 || zV==0){
//                System.out.println("Hibás adatot adtál meg!");
        double averageResult=(firstZh+secondZh+zV)/3.0;
        strudentDao.saveToDb(name, averageResult);
        
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
