/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tbiro
 */
@WebServlet(name = "Palindrom", urlPatterns = {"/Palindrom"})
public class Palindrom extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Palindrom</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Palindrom at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String string = req.getParameter("string");
        System.out.println(string);
//        String[] splittedString = string.split("");
        boolean isPolindrom;
        StringBuilder sb = new StringBuilder();
        
        for (int i = string.length()-1; i >= 0; i--) {
            System.out.println(string.charAt(i));
            sb.append(string.charAt(i));
        }
        String reverseString = sb.toString();
        System.out.println("A kapott szöveg fordítottja: "+reverseString);
        
        if (string.equals(reverseString)) {
            System.out.println("Ez biza polindrom!");
            isPolindrom=true;
        } else {
            System.out.println("NEM polindrom!");
            isPolindrom=false;
        }
        req.setAttribute("isPolindrom", isPolindrom);
        
        //küldöm response-ban az "ítéletet"
        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Response</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Ez polindrom? " + req.getAttribute("isPolindrom") + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
