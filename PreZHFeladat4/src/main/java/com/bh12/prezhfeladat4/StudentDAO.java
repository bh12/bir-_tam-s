/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.prezhfeladat4;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tbiro
 */

public class StudentDAO {
    
    @PersistenceContext
    EntityManager em;
    
    public void saveToDb (String name,double avergaResult){
        Student student = new Student(name, avergaResult);
        em.persist(student);
    }
}
