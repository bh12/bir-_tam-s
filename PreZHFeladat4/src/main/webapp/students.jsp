<%-- 
    Document   : students
    Created on : 2020.09.24., 18:33:25
    Author     : tbiro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="ResultServlet" method="POST">
            <label for="string">Hallgató neve:</label><br>
            <input type="text" id="name" name="name"><br>
            <label for="number">Első ZH eredménye</label><br>
            <input type="text" id="firstZh" name="firstZh"><br>
            <label for="number">Második ZH eredménye</label><br>
            <input type="text" id="secondZh" name="secondZh"><br>
            <label for="number">Záróvizsga eredménye</label><br>
            <input type="text" id="zV" name="zV"><br>
            <input type="submit" value="Submit">
        </form> 
    </body>
</html>
