<%-- 
    Document   : Booking
    Created on : 2020.06.17., 10:46:29
    Author     : tbiro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ezzel adhatsz hozzá foglalásokat!</h1>
        <form action="BookingServlet" method="post">
            <label for="fname">Start Date</label><br>
            <input type="date" id="StartDate" name="startDate"><br>

            <label for="lname">End Date</label><br>
            <input type="date" id="endDate" name="endDate"><br>

            <label for="fname">Car Id</label><br>
            <input type="number" id="carId" name="carId"><br>

            <input type="submit" value="Submit">
        </form> 
    </body>
</html>
