/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WelcomeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

    
@WebServlet(name = "Welcome", urlPatterns = {"/Welcome"}) //végont!
public class Welcome extends HttpServlet {

  
    /*
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Welcome</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Welcome Tomi at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    */
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) //ez rakja ki a weboldalra?
            throws ServletException, IOException {
        request.getRequestDispatcher("Car.jsp").forward(request,response); //kérés továbbítás egy jsp oldalra
        //request.getRequestDispatcher("Welcome").forward(request,response); //kérés továbbítás egy jsp oldalra
        /*
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet WelcomeServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<form action=\"WelcomeServlet\" method=\"post\">\n"
                    + "  <label for=\"fname\">Email address:</label><br>\n"
                    + "  <input type=\"text\" id=\"email\" name=\"email\" \"><br>\n"
                    + "  <label for=\"lname\">Password:</label><br>\n"
                    + "  <input type=\"text\" id=\"password\" name=\"password\" \"><br><br>\n"
                    + "  <input type=\"submit\" value=\"Submit\">\n"
                    + "</form> ");
            out.println("</body>");
            out.println("</html>");
        }
    */
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) //ez pedig beszedi az adatokat?
            throws ServletException, IOException {
        String email = request.getParameter("email");//fontos a pontos "email" paraméter
        String password = request.getParameter("password");
        
        PrintWriter pw = response.getWriter();
        
        pw.println(email);
        pw.println(password);
        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
