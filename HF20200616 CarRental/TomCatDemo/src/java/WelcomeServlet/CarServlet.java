package WelcomeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost/Blood?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) {
        
        res.setContentType("text/html");

        HttpSession session = req.getSession();

        Integer counter = (Integer) session.getAttribute("counter");

        if (counter == null) {
            session.setAttribute("counter", 1);
        } else {
            session.setAttribute("counter", ++counter);
        }

        Integer bookingId = req.getParameter("id") == null ? -1 : 
                Integer.valueOf(req.getParameter("id"));
        
        try (PrintWriter out = res.getWriter();
                Connection conn = DriverManager.getConnection(CONNECTION_URL, "root", "Bt17510205");
                Statement st = conn.createStatement();) {
            Class.forName("com.mysql.cj.jdbc.Driver");

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CarServlet #" + counter + "</title>");
            out.println("</head>");
            out.println("<body>");

            String query = "select * from Donors";

            if (bookingId != -1) {
                query += " where id = " + bookingId;
            }

            ResultSet rs = st.executeQuery(query);
            out.println("<p> Id:"+bookingId+"</p>");
            out.println("<table>\n"
                    + "  <tr>\n"
                    + "    <th>id</th>\n"
                    + "    <th>Details</th>\n"
                    + "  </tr>\n");
            while (rs.next()) {
                out.println(
                        "  <tr>\n"
                        + "    <td>" + rs.getInt(1) + "</td>\n"
                        + "    <td>" + rs.getString(2) + "</td>\n"
                        + "  </tr>\n"
                );
            }

            out.println("</table></body>");
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            System.out.println("Valami class not found hiba!");
        }
    }

}
