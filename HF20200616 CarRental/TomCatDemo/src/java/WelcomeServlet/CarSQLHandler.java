/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WelcomeServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tbiro
 */
@WebServlet(name = "CarSQLHandler", urlPatterns = {"/CarSQLHandler"})
public class CarSQLHandler extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CarSQLHandler</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CarSQLHandler at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       request.getRequestDispatcher("CarAdding.jsp").forward(request,response);
    }
    private static final String CONNECTION_URL = "jdbc:mysql://localhost/Car?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        //request.getRequestDispatcher("Car.jsp").forward(request,response);
        
        String plateNumber = request.getParameter("plateNumber");
        String color = request.getParameter("color");
        String engineType = request.getParameter("engineType");
        String weigthString = request.getParameter("weigth");
        String yearOfBuiltString = request.getParameter("yearOfBuilt");
        String siteIdString=request.getParameter("siteId");
        
       
       int weigth =Integer.parseInt(weigthString);
       int yearOfBuiltDate = Integer.parseInt(yearOfBuiltString);
       int siteId = Integer.parseInt(siteIdString);
       
        ///ez akkor kell, ha ki szeretnék a weboldalra írni adatokat!
        PrintWriter pw = response.getWriter();
        pw.println("Az adatbázisba felvettem az adatokat!");
        pw.println("<a href=\"MainMenuServlet\" >vissza a manübe!</a>");
        response.setContentType("text/html");
        
        
        //HttpSession session = request.getSession(); //ez sem kell!
          /*
           Integer carId = request.getParameter("id") == null ? -1 : 
                Integer.valueOf(request.getParameter("id"));
        */
          
        try (PrintWriter out = response.getWriter();
                Connection conn = DriverManager.getConnection(CONNECTION_URL, "root", "Bt17510205");
                PreparedStatement ps = conn.prepareStatement("INSERT INTO car (PlateNumber,Color,EngineType,Weight,YearOfBuilt,SiteId) VALUES (?,?,?,?,?,?)")) {
                //ps.setInt(1, carId);//ez auto increment, emiatt nem kell!
                ps.setString(1,plateNumber);
                ps.setString(2,color);
                ps.setString(3,engineType);     
                ps.setInt(4,weigth);
                ps.setInt(5, yearOfBuiltDate);
                ps.setInt(6,siteId);
                
                ps.executeUpdate();
                
    }   catch (SQLException ex) {
            Logger.getLogger(CarSQLHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
