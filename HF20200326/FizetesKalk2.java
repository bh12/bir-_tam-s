/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizeteskalk2;

/**
 *
 * @author tbiro
 */
public class FizetesKalk2 {

    public static void main(String[] args) {
        int employeeNumber = 10;
        int[][] employeeData = new int[employeeNumber][13];
        for (int i = 0; i < employeeData.length; i++) {
            for (int j = 1; j < employeeData[0].length; j++) {
                employeeData[i][0] = i + 1;
                if (j % 3 == 0) {
                    employeeData[i][j] = (450_000 + calculateRandom(50000, 125000)) * (calculateRandom(5, 15)/100+1);
                } else {
                    employeeData[i][j] = 450_000 + calculateRandom(50000, 125000);
                }
            }
        }
        String a,b;
        if (args.length<2){
        a = "Laci,Józsi,Béla,Feri,Adri,Zsófi,Károly";
        b = "Nagy,Veréb,Molnár,Kiss,Papp,Tóth";
        }
        else {
            a=args[0];
            b=args[1];
        }
        String names[]=nameMaker(a,b,employeeData);
        printData(employeeData,names);
    }

    public static int calculateRandom(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }

    public static void printData(int[][] employeeData,String[] names) {
        for (int i = 0; i < employeeData.length; i++) {
            int yearlySalary=0;
            System.out.println("Személy azonosító: " + employeeData[i][0]);
            System.out.println("Név: "+names[i]);
            for (int j = 1; j < employeeData[0].length; j++) {
                System.out.println(j + ".havi fizetés: " + employeeData[i][j]);
                yearlySalary+=employeeData[i][j];
            }
        System.out.println("Éves fizetése: "+yearlySalary+"\n");
        }
    }
    public static String [] nameMaker (String a, String b,int[][]employeeData){ 
        String[] secondNames = a.split(",");
        String[] firstNames = b.split(",");
        String[] names = new String [employeeData.length];
        for (int i=0;i<employeeData.length;i++){
            names[i]=firstNames[calculateRandom(0,firstNames.length)]+" "+
                    secondNames[calculateRandom(0,secondNames.length)];
            
        }
        return names;
        
        /*for (String s : stringArray) {
            System.out.println(s);
        }*/
}
}
