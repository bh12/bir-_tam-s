/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programlinearguments;

/**
 *
 * @author tbiro
 */
public class ProgramLineArguments {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Adjál meg több paramétert!");
            return;
        }
        for (int i=0;i<args.length;i++){
            if ("pig".equals(args[i])) {        
                System.out.println("Malacod van!");
            }
        }
        
        int sum = 0; // számösszegük
        for (String argString : args) {
            if (!"pig".equals(argString)){ 
            int number = Integer.parseInt(argString);
            sum += number;
        }
        }
        System.out.println("A megadott számok összege: " + sum);

        int randomIndex = (int) (Math.random() * args.length-1); //véletlen szám a tömbből
        int randomNumber = Integer.parseInt(args[randomIndex]);
        System.out.println("Egy véletlen szám a parancssorból: " + randomNumber);

 
    }
}
