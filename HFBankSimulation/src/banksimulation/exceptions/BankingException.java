package banksimulation.exceptions;

import banksimulation.model.Bank;
import banksimulation.model.Transfer;

public class BankingException extends Exception {

    private final Transfer transfer;

    public BankingException(String errorDetails, Transfer transfer) {
        super(errorDetails);
        this.transfer = transfer;
     }

    public Transfer getTransfer() {
        return transfer;
    }

    
}
