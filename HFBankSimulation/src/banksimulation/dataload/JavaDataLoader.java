
package banksimulation.dataload;

import banksimulation.BankApplication; //importál egy mappa.osztály szerkezetet, így éri el később a változoit, pl. list banks
import static banksimulation.BankApplication.banks;
import banksimulation.model.Bank;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class JavaDataLoader implements DataLoader {

    @Override
    public void loadInitialData() {
        for (int i=0;i<5;i++){
            Bank refBank=generateRandomBanks(i); 
          //BankApplication.banks.add(generateRandomBanks(i));  
          List<Client> refClients= new ArrayList<>();
           for (int j = 0; j < 10; j++) {
            
            refClients.add(generateRandomClient());
            
            //BankApplication.clients.add(generateRandomClient());
        }
           refBank.setClients(refClients);
           List<Transfer> refTansfers = new ArrayList<>();
           for (int j = 0; j < 10; j++) {
               
               refTansfers.add(generateTransfer(refBank.getClients()));
               
            //BankApplication.transfers.add(generateTransfer(BankApplication.clients));
        }
           refBank.setTransfers(refTansfers);
           banks.add(refBank);
        }
    }
    
    public static Transfer generateTransfer(List<Client> clients) {
        Transfer t = new Transfer();

        int randomSourceIndex = (int) (Math.random() * clients.size());
        int randomTargetIndex = (int) (Math.random() * clients.size());

        t.setSource(clients.get(randomSourceIndex));
        t.setTarget(clients.get(randomTargetIndex));

        long ammount = (long) (Math.random() * BankApplication.MAX_TRANSFER_AMMOUNT);

        t.setAmmount(ammount);

        t.setDateOfCompletion(new Date());

        return t;
    }

    public static Bank generateRandomBanks(int i){
        Bank bank = new Bank ("Bank"+i);
        return bank;
    }
    
    public static Client generateRandomClient() {
        long balance = (long) (Math.random() * 100000 + BankApplication.CLIENT_BASE_BALANCE);
        String clientId = generateRandomClientId();
        String accountNumber = generateRandomAccountNumber();

        Client c = new Client(clientId, accountNumber, balance);
        c.balanceList.add(balance);
        return c;
    }

    public static String generateRandomString(int length, int min, int max) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            char randomChar = (char) (Math.random() * (max - min + 1) + min);
            sb.append(randomChar);
        }

        return sb.toString();
    }

    public static String generateRandomClientId() {
        return generateRandomString(5, 65, 90);
    }

    public static String generateRandomAccountNumber() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 15; i++) {
            char randomChar = (char) (Math.random() * 46 + 48);
            sb.append(randomChar);
        }

        return generateRandomString(15, 48, 90);
    }
}
