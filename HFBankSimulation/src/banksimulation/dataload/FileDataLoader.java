package banksimulation.dataload;

import banksimulation.BankApplication;
import static banksimulation.BankApplication.banks;
import banksimulation.model.Bank;
import banksimulation.model.Client;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileDataLoader implements DataLoader { //még nincs készen arra a megoldásra, hogy bankonként beolvassa file-ból azadatokat

    @Override
    public void loadInitialData() {
        Scanner sc= new Scanner (System.in);
        System.out.println("Melyik bank adatait töltsem be?");
        String bankName=sc.nextLine();
        File clientFile = new File(bankName+"_allclients.txt"); 
        Bank tempBank= new Bank(bankName);
        

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            List<Client> tempClients = new ArrayList<>();
            while (line != null) {
               
                String[] clientArray = line.split(" ");
                String clientId = clientArray[0];
                String accountNumber = clientArray[1];
                long balance = Long.parseLong(clientArray[2]);
                Client c = new Client(clientId, accountNumber, balance);
                tempClients.add(c);
                
                line = br.readLine();
            }
            tempBank.setClients(tempClients);
            banks.add(tempBank);
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
    }

}
