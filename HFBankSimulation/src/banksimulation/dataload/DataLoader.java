
package banksimulation.dataload;

public interface DataLoader {
    
    public void loadInitialData();
    
}
