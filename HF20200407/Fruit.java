/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200407fruits;

/**
 *
 * @author tbiro
 */
public  class Fruit {
    private String color;
    private double price;
       
  //public abstract String valamilyenMetodus();

    public Fruit(String color, double price) {
        this.color = color;
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public double getPrice() {
        return price;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Fruit{" + "color=" + color + ", price=" + price + '}';
    }
    
}
