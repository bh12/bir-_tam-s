/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200407fruits;

/**
 *
 * @author tbiro
 */
public class Orange extends Fruit {

    public Orange(String color, double price) {
        super("orange", price);
    }

    @Override
    public void setColor(String color) {
        System.out.println("A narancs csak narancs színű lehet!");
        super.setColor("orange");

    }

}
