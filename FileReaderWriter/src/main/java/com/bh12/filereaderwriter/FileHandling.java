/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh12.filereaderwriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.lineSeparator;

/**
 *
 * @author tbiro
 */
public class FileHandling {

    public static void main(String[] args) {
        File file = new File("sentences.txt");
        File filetoWrite = new File("toFile.txt");
        try {
            filetoWrite.delete();
            readEvery2ndline(file);
        } catch (IOException ex) {
            System.out.println("Nem érem el a file-T!");
        }
    }

    public static void readEvery2ndline(File file) throws IOException {
        try (FileReader fr=new FileReader (file);
                BufferedReader br = new BufferedReader(fr);)
                {
            String line = br.readLine();
            int lineCounter=1;
            while (line!=null) {
                if (lineCounter%2==0){
                    writeToFile(line);
                }
                line = br.readLine();
                lineCounter++;
             }
        }

    }     
    public static void writeToFile (String line) throws IOException{
         try (FileWriter fw=new FileWriter ("toFile.txt",true);
                BufferedWriter bw = new BufferedWriter(fw);)
                {
                    bw.write(line);
                    bw.write(lineSeparator());
                }
    }
}
