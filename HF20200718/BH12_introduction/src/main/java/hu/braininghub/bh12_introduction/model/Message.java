/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12_introduction.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author tbiro
 */
@Entity //ez csinál ebből az osztályból entity bean-t (entitás Bean)
@Table (name="message") //message objektum megfeleltetése az adatbázisban levő message oszloppal

public class Message { //ez egy entitás bean lesz!
    
    @Id
    @Column(name="id") //ez a megfeleltetés az oszlop névben az adatbázissal
    private int id; //ezt létre kell hozni, mert ...?
    private String email;
    private String subject;
    private String message;
 
    public int getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getMessage() {
        return message;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
