package hu.braininghub.bh12_introduction.servlets;

import hu.braininghub.bh12_introduction.service.MessageService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ListMessage extends HttpServlet { //ez a servlet akkor indul el, ha a weboldalra beírjuk /ListMessage
    //private MessageService service = new MessageService(); //ez volt a régi kód
    
    @Inject //ettől már Bean lesz!
    private MessageService service; //ezzel Ő példányosítja ezt saját maga, nem mi csináljuk
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("messageList", service.getMessages());
        
        request.setAttribute("messageById", service.getMessageById(1)); //itt az első üzenetet keresi meg
        request.getRequestDispatcher("WEB-INF/list_message.jsp").forward(request, response); //továbblép ide
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
