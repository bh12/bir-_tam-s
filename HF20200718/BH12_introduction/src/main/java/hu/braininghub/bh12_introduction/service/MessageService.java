package hu.braininghub.bh12_introduction.service;

import hu.braininghub.bh12_introduction.model.Message;
import hu.braininghub.bh12_introduction.model.MessageDAO;
import hu.braininghub.bh12_introduction.model.MessageDTO;
import hu.braininghub.bh12_introduction.model.MessageRepository;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless //ettől már Bean lesz
public class MessageService {
    //MessageDAO dao = new MessageDAO();
    
    @Inject //beszúrja az adott bean-t a JavaEE (Az @ejb is hasnlót csinál)
    MessageDAO dao; //ez példányosít
    public void addMessage(MessageDTO message) {
        MessageRepository.addMessage(message);
    }
    public void addMessage(String email, String subject, String message) {
        MessageRepository.addMessage(new MessageDTO(email, subject, message)); //mivel ez static, nem kell példányosítani itt!
        try {
            dao.addNewMessage(email, subject, message);
        } catch (SQLException ex) {
            Logger.getLogger(MessageService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public List<MessageDTO> getMessages() {
        /*
        try {
            return dao.getMessages();
            //return MessageRepository.getMessages();
        } catch (SQLException ex) {
            Logger.getLogger(MessageService.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        return dao.getMessages();
    }
    public Message getMessageById(int id){
        return dao.getMessageById(id);
    }
    public void deleteMessage(int id) { //ez csak áthív egy másik osztályba!! Csak DAO-ból szabad törölni adatbázis sort!
       dao.deleteMessage(id);
   }
    public void modifyMessage (int id, String email, String subject, String message){
        dao.modifyMessage(id, email, subject,  message);
    }
}
