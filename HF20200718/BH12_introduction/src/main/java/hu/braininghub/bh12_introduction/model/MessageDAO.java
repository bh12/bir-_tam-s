package hu.braininghub.bh12_introduction.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class MessageDAO { //alkalmazásszerverben definiált jdbc connection pool fog működni. (persistance.xml kell)
    @PersistenceContext
    EntityManager em; //ez felelős az adatbázis "kapcsolatért"?

    public MessageDAO() { //üres construktor
    }
    
   public void addNewMessage(String email, String subject, String message) throws SQLException { //ezt szúrtuk be órán, ez rakja bele az adatbázisba
       Message m=new Message();
       m.setEmail(email);
       m.setMessage(message);
       m.setSubject(subject);
       em.persist(m); //ez a lényeg! Ez hajtja végre az adatbázisba írást!
   }
   public Message getMessageById (int id) { //ki kell próbálni pl. email kereséssel
        Message m = em.find(Message.class, id);
        return m;
}
   public List<MessageDTO> getMessages(){ //egy mapper metódushívással az összes üzenetet kiszedi az adatbázisból és DTO-vá alakítja
       List<Message> messages = em.createQuery("SELECT m FROM Message m").getResultList(); //JPQL nyelv. az 'm' itt az alias
            //ha jól értem, ez minden üzenetet kiment az adatbáziból egy listába.
       List<MessageDTO> messageDTOs = new ArrayList();
       for (Message message : messages){
           messageDTOs.add(MessageMapper.toDTO(message));
       }
       return messageDTOs;
   }
   public void deleteMessage(int id)  { 
       Message m=this.getMessageById(id); 
       em.remove(m); //ez töröl az adatbázisból!
   }
   public void modifyMessage (int id, String email, String subject, String message){
       Message m=this.getMessageById(id);
       if (!email.equals("")) m.setEmail(email);
       if (!subject.equals("")) m.setSubject(subject);
       if (!message.equals("")) m.setMessage(message);
       em.merge(m);
   }
}