/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12_introduction.servlets;

import hu.braininghub.bh12_introduction.service.MessageService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tbiro
 */
@WebServlet(name = "DeleteAndModify", urlPatterns = {"/DeleteAndModify"}) //ezt azért hoztuk létre, hogy legyen a törlésre servlet
public class DeleteAndModify extends HttpServlet {

    @Inject
    private MessageService service; //beinjektáljuk a service példányt
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.valueOf(request.getParameter("messageId")); //ez szedi ki az id-t
     
        service.deleteMessage(id); //nem szabad innen egyből törölni, ezért kell a service 'réteg'
        //service.modifyMessage(id); //csak próba módosítás
        response.sendRedirect(request.getContextPath() + "/index.jsp"); //tovább kell küldeni, hogy lássuk a változást
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response); //általános esetben így küldjük át...
        int choosenId = Integer.valueOf(request.getParameter("id"));
        String email = request.getParameter("email"); 
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");
        System.out.println(email+","+subject+","+message);
        service.modifyMessage(choosenId, email, subject, message);
        
    response.sendRedirect(request.getContextPath() + "/index.jsp"); //tovább kell küldeni, hogy lássuk a változást
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
