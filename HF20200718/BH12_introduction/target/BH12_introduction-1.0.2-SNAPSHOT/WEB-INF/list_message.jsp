<%@page import="java.util.List"%>
<%@page import="hu.braininghub.bh12_introduction.model.MessageDTO"%>
<%@page import="hu.braininghub.bh12_introduction.model.Message"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Referenciák</title>

        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="container">
            <%@include file="menu.jsp" %>
           

            <div class="row">
                <h1>Válaszd ki melyik üzenet sort módosítsuk!</h1>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Button</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${messageList}" var="message"> <!-- csak listával működik, ld messageList, a var= az amivel végigiterál -->
                            <tr>
                                
                                <td>${message.id}</td>
                                <td>${message.email}</td>
                                <td>${message.subject}</td>
                                <td>${message.message}</td>
                                <td><a class="button" href="DeleteAndModify?messageId=${message.id}">Delete the line</a></td>                                                               
                            </tr>
                        </c:forEach>                           
                    </tbody>
                </table>
                <h1>Add meg mit módosítsunk!</h1>
                <br>
                <form method="POST" action="DeleteAndModify" onsubmit="return validateForm()">
                    <div class="form-group">
                        <label for="id">Id:</label>
                        <input type="number" class="form-control" placeholder="which Id?" id="id" name="id">
                    </div>
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" placeholder="Enter new email" id="email" name="email" onkeypress="emailKeyPressed()" onchange="emailChange()">
                </div>
                <div class="form-group">
                    <label for="subject">Subject:</label>
                    <input type="text" class="form-control" placeholder="Enter new subject" name="subject" id="subject">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea id="message" class="form-control" name="message"> 
                    </textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
    </body>
</html>
