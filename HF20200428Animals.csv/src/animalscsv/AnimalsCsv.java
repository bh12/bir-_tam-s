/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalscsv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AnimalsCsv {

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();

        //boolean isEndofFile=false;
        //while (isEndofFile=false){
        try (FileReader fr = new FileReader("animals.csv");
                BufferedReader br = new BufferedReader(fr)){
                
            String line = br.readLine(); //honnan tudja, hogy melyik sor jön???
            String headLine = line;
            int lineCount = 0;
            while (line != null) { //innen számolja a sorokat
                lineCount++;
                sb.append(line);
                line = br.readLine();
            }
            //String[][] dataBase = new String[lineCount][headerCount(headLine)];//2D tömb létrehozása headerCount(headLine) 
            CreateDatabase(br);
        } catch (IOException ex) {
            System.out.println("Hiba van!");
        }

    }

    public static void CreateDatabase(final BufferedReader br) throws IOException {
        String[][] dataBase = new String[101][18];
        BufferedReader secondReader = br;
        //BufferedReader secondReader = new BufferedReader(fr); //később megnézem, hogy a fenti br használható-e még!
        String tempLine;
        for (int i = 0; i < dataBase.length; i++) {
            tempLine = secondReader.readLine();
            System.out.println(tempLine);
            String[] tempString = tempLine.split(";");
            for (int j = 0; j < dataBase[i].length; j++) {
                dataBase[i][j]=tempString[j];
                System.out.println(dataBase[i][j]);
            }
        }
        
        /*
        System.out.println("1. feladat: Állatok száma: " + (lineCount - 1));
        //System.out.println("Első sor: "+headLine);
        System.out.println("2. Feladat. Ennyi tulajdonság van: " + headerCount(headLine));
        int getColumn = searchHeadLine(headLine, "hair");//melyik oszlopban van a keresett tuajdonság?
    */  }

    public static int headerCount(String line) { //megszámolja, hogy az első sorban mennyi mező van
        String[] tempString = line.split(";");
        return tempString.length;
    }

    public static int searchHeadLine(String headLine, String text) {
        String[] tempString = headLine.split(";");
        for (int i = 0; i < tempString.length; i++) {
            if (tempString[i].equals(text)) {
                return i;
            }
        }
        return -1; //lehetne kivételben is?
    }

    /*public static int[][] makeDatabase(BufferedReader br) {
        
    }*/

}
