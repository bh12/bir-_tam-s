/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200418varosok;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Country {
    private String cName;
    public List<City> cities = new ArrayList<>();
    
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj; // mi ez az other?
        if (!Objects.equals(this.cities, other.cities)) { // itt kellene az összes várost ellenőrízni
            return false;
        }
        return true;
    }

    public Country(String cName) {
        this.cName = cName;
    }

   
    
}
