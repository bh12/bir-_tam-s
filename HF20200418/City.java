/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200418varosok;

/**
 *
 * @author tbiro
 */
public class City {
    private int millionPopulation;
    private String name;

    public City(int millionPopulation, String name) {
        this.millionPopulation = millionPopulation;
        this.name = name;
    }

    public int getMillionPopulation() {
        return millionPopulation;
    }

    public String getName() {
        return name;
    }

    public void setMillionPopulation(int millionPopulation) {
        this.millionPopulation = millionPopulation;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name=" + name + ", City millionPopulation=" + millionPopulation;
    }
    
    
}
