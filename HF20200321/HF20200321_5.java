/*
5. fibonacci sorozat n. elemének kiszámolására, oldja meg hagyományos és rekurzív módon
 */
package hf20200321_5;

/**
 *
 * @author tbiro
 */
public class HF20200321_5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n = 25;
        System.out.println("A Fibonacci sorozat "+n+". eleme: " + fibonacci(n));
    }

    public static long fibonacci(int n) {
        long[] array = new long[n+1];
        switch (n) {
            case 0:
                break;
            case 1:
                array[1] = 1;
                break;
            default:
                array[1] = 1;
                for (int i = 2; i <= n; i++) {
                    array[i] = array[i - 1] + array[i - 2];
                }
        }
        return array[n];
    }

}
