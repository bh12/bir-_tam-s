/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200321_3;

/**
 *
 * @author tbiro
 */
public class HF20200321_3 {

    public static void main(String[] args) {
        int min=10;
        int max=99;
        int arraySize=10;
        int[][] array1 = fillWithRandoms(arraySize, min, max);
        int[][] array2 = fillWithRandoms(arraySize, min, max);
        // listMatrix(array1);listMatrix(array2); // teszt
        int[][] sumMatrix =sumMatrix(array1,array2);
        listMatrix(sumMatrix);
    }
    public static int[][] fillWithRandoms(int size, int min, int max) {
        int[][] array = new int[size][size];
        for (int i = 0; i < array.length; i++) {
            for (int j=0;j<array[i].length;j++){
              array[i][j] = (int) (Math.random() * (max+1 - min) + min);  
            }
        }
        return array;
}
    public static int [][] sumMatrix (int[][] array1,int[][] array2){
        int[][]sumMatrix =new int [array1.length][array1[0].length]; // a két mátrix egyforma (négyzet alakú is), tehát mindegy melyik hosszát nézzük a továbbiakban
        for (int i = 0; i < array1.length; i++) {
            for (int j=0;j<array1[i].length;j++){
              sumMatrix[i][j] = array1[i][j]+ array2[i][j];
            }
        }
         return sumMatrix;
    }
   public static void listMatrix (int [][] matrix){
       System.out.println("A mátrix elemei:");
       for (int i = 0; i < matrix.length; i++) {
            for (int j=0;j<matrix[i].length;j++){
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println("");
        }
   }
   
}
