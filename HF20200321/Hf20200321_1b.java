/*
1. A program dolgozzon egy 20 elemű tömbbel, töltse fel véletlen kétegyű számokkal! Minden részfeladatot külön metódusba kell megvalósítani
    - Listázza a tömbelemeket
    - Páros tömbelemek összege
    - Van-e öttel osztható szám
    - Melyik az első páratlan szám a tömbben
    - Van-e a tömbben 32
 */
package hf20200321_1b;

/**
 *
 * @author tbiro
 */
public class Hf20200321_1b {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] array = fillWithRandoms(20);
        listArray(array);
        sumOfEvens(array);
        isDivider(array, 5);
        firstUnMate(array);
        isNumberIn(array,32); 
    }

    public static int[] fillWithRandoms(int size) {
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (100 - 10) + 10);
        }
        return array;
    }

    public static void listArray(int[] array) {
        System.out.println("A tömb elemei:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("\n");
    }

    public static void sumOfEvens(int[] array) {

        int sumOfEvens = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                sumOfEvens += array[i];
            }
        }
        System.out.println("A páros tömbelemek összege: "+sumOfEvens);
    }

    public static void isDivider(int[] array, int divider) {
        boolean is5Divider=false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % divider == 0) {
                System.out.println("A tömbben van "+ divider+"-tel osztható szám!");
                is5Divider=true;
                break;
            }
        }
        if (is5Divider==false){
        System.out.println("A tömbben nincs "+ divider+"-tel osztható szám!");
        }
    }

    public static void firstUnMate(int[] array) {
        boolean isUnMate=false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1) {
                System.out.println("Az első páratlan szám a tömbben: "+array[i]);
                isUnMate=true;
                break;
            }
        }
        if (isUnMate==false) {
            System.out.println("Nincs páratlan szám a tömbben!");
            }
        }
    public static void isNumberIn(int[] array, int number) {
        boolean is32in=false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                System.out.println("Van "+number+" a tömbben!");
                is32in=true;
                break;
            }
        }
        if (is32in==false){
            System.out.println("Nincs "+number+" a tömbben!");
        }
    }
}
